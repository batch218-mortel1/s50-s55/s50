import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import CourseCard from '../components/CourseCard'

import {Container} from 'react-bootstrap';

export default function Home() {
	return (
		<>
		<Banner />
        <Highlights />
        <CourseCard />
		</>

	)
}