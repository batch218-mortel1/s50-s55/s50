import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';

import {Container} from 'react-bootstrap';

import './App.css';

function App() {
  return (
    <>
    <AppNavbar />
    <Container> 
      <Home />
    </Container>
    </>
  );
}

export default App;